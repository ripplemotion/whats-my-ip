from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(name='whats-my-ip',
      version='0.1',
      description='HTTP server that returns requestee IP address as plain/text',
      long_description=readme(),
      author='Olivier Tabone',
      author_email='olivier.tabone@ripplemotion.fr',
      packages=['whats_my_ip'],
      install_requires=[
          'flask',
      ],
      entry_points={
          'console_scripts': ['whats-my-ip-server=whats_my_ip:main'],
      },
      include_package_data=True,
      zip_safe=False)
