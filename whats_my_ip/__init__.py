import argparse
from flask import Flask
from flask import request
from flask import make_response

app = Flask(__name__)


@app.route("/")
def hello():
    resp = make_response(request.remote_addr)
    resp.headers['Content-Type'] = 'text/plain; charset=utf-8'
    return resp


def main():
    parser = argparse.ArgumentParser(description='Serve an HTTP application that returns requestee IP address.')
    parser.add_argument('--port', dest='port',
                        type=int, default=5000,
                        help='TCP port to bind to')
    args = parser.parse_args()
    app.run(host='0.0.0.0', port=args.port)
